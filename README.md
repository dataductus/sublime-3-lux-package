# README #

Basic syntax highlighting and functionality for Lux (LUcid eXpect scripting).

### Installation ###

Copy 'Lux.sublime-package' to '~/.config/sublime-text-3/Installed Packages'

### Configuration ###

Command name: "lux_goto".

Example keymap:
[
    { 
        "keys": ["f12"], 
        "command": "lux_goto" 
    }
]

Example mousemap:
[
    {
        "button": "button1",
        "modifiers": ["ctrl"],
        "press_command": "drag_select",
        "command": "lux_goto"
    }
]
