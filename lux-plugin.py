import sublime
import sublime_plugin
import re
import os


class LuxGotoCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        relative_filename = self.get_included_filepath()
        if relative_filename is not None:
            current_directory = os.path.dirname(os.path.abspath(self.view.file_name())) + "/"
            self.view.window().open_file(current_directory + relative_filename)
            return

        current_macro = self.get_macro_name()
        if current_macro is not None:
            files = [os.path.abspath(self.view.file_name())]
            while len(files) > 0:
                fn = files.pop()
                current_directory = os.path.dirname(fn) + "/"
                macros, includes = self.find_macros_in_file(fn)
                for (ln, macro) in macros:
                    if macro == current_macro:
                        self.view.window().open_file("%s:%d" % (fn, ln), sublime.ENCODED_POSITION)
                        return
                for relative_path in includes:
                    files.append(current_directory + relative_path)

    def is_enabled(self):
        return self.get_included_filepath() is not None or self.get_macro_name() is not None

    def get_macro_name(self):
        for region in self.view.sel():
            line = self.view.line(region)
            line_contents = self.view.substr(line).strip()

        include_pattern = re.compile('\[invoke (\S+).*\]')
        m = include_pattern.match(line_contents)
        if m is not None and len(m.groups()) == 1:
            macro_name = m.group(1)
            return macro_name

        return None

    def get_declared_macro_name(self, line):
        include_pattern = re.compile('\[macro (\S+).*\]')
        m = include_pattern.match(line)
        if m is not None and len(m.groups()) == 1:
            macro_name = m.group(1)
            return macro_name

        return None

    def get_included_filepath(self):
        for region in self.view.sel():
            line = self.view.line(region)
            line_contents = self.view.substr(line).strip()
        return self.get_included_file(line_contents)

    def get_included_file(self, line):
        include_pattern = re.compile('\[include (\S+)\]')
        m = include_pattern.match(line)
        if m is not None and len(m.groups()) == 1:
            relative_filename = m.group(1)
            return relative_filename

        return None

    def find_macros_in_file(self, fn):
        macros = []
        includes = []

        with open(fn) as f:
            ln = 0
            for line in f:
                ln += 1
                macro_name = self.get_declared_macro_name(line)
                if macro_name is not None:
                    macros.append((ln, macro_name))
                else:
                    included_file = self.get_included_file(line)
                    if included_file is not None:
                        includes.append(included_file)

        return macros, includes
